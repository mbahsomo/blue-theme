<?php

/**
 * This is the model class for table "tbl_news".
 *
 * The followings are the available columns in table 'tbl_news':
 * @property integer $n_id
 * @property string $n_title
 * @property string $n_isi
 * @property string $user_entry
 * @property string $date_entry
 * @property string $date_edit
 */
class News extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return News the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbl_news';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('n_title', 'length', 'max'=>100),
			array('user_entry', 'length', 'max'=>15),
			array('n_isi, date_entry, date_edit', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('n_id, n_title, n_isi, user_entry, date_entry, date_edit', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'n_id' => 'N',
			'n_title' => 'N Title',
			'n_isi' => 'N Isi',
			'user_entry' => 'User Entry',
			'date_entry' => 'Date Entry',
			'date_edit' => 'Date Edit',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('n_id',$this->n_id);
		$criteria->compare('n_title',$this->n_title,true);
		$criteria->compare('n_isi',$this->n_isi,true);
		$criteria->compare('user_entry',$this->user_entry,true);
		$criteria->compare('date_entry',$this->date_entry,true);
		$criteria->compare('date_edit',$this->date_edit,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}