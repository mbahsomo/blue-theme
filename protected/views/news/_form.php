<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'news-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'n_title'); ?>
		<?php echo $form->textField($model,'n_title',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'n_title'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'n_isi'); ?>
		<?php echo $form->textArea($model,'n_isi',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'n_isi'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'user_entry'); ?>
		<?php echo $form->textField($model,'user_entry',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'user_entry'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'date_entry'); ?>
		<?php echo $form->textField($model,'date_entry'); ?>
		<?php echo $form->error($model,'date_entry'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'date_edit'); ?>
		<?php echo $form->textField($model,'date_edit'); ?>
		<?php echo $form->error($model,'date_edit'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->