<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('n_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->n_id), array('view', 'id'=>$data->n_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('n_title')); ?>:</b>
	<?php echo CHtml::encode($data->n_title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('n_isi')); ?>:</b>
	<?php echo CHtml::encode($data->n_isi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_entry')); ?>:</b>
	<?php echo CHtml::encode($data->user_entry); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date_entry')); ?>:</b>
	<?php echo CHtml::encode($data->date_entry); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date_edit')); ?>:</b>
	<?php echo CHtml::encode($data->date_edit); ?>
	<br />


</div>