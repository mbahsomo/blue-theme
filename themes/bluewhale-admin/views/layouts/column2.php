<?php $this->beginContent('//layouts/main'); ?>

<!-- menu Kiri -->
<div class="grid_2">
    <div class="box sidemenu">
        <div class="block" id="section-menu">
        	<?php
			$this->beginWidget('zii.widgets.CMenu', array(
				'items' => $this->menu,
				'encodeLabel'=>false,
				'htmlOptions' => array('class' => 'section menu'),
			));
			$this->endWidget();
			?>
            <!--ul class="section menu">
                <li><a class="menuitem">Satu</a></li>
				<li><a class="menuitem">Dua</a></li>                        
            </ul-->
        </div>
    </div>
</div>
<!-- Content -->
<div class="grid_10">
    <div class="box round first">
        <h2>Buttons</h2>
        <div class="block">
            <?php echo $content; ?>
        </div>
    </div>
</div>

<?php $this->endContent(); ?>
