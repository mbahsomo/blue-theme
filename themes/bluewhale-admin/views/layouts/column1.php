<?php $this->beginContent('//layouts/main'); ?>
<!-- Title area -->
<div class="grid_12">
    <div class="box round first fullpage">
        <h2>Form Controls</h2>
        <div class="block ">
            <?php echo $content; ?>
        </div>
    </div>
</div>
<?php $this->endContent(); ?>